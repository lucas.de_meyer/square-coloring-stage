import cvxpy as cp
import numpy as np
import scipy
from scipy.optimize import linprog
from cvxopt import matrix, solvers


## Path of the files
# Main path
path = '.'

# Path to the list of all configurations
name_pent = path +'/List/save_penta2'
name_tri = path + '/List/save_tri2'
name_3ver = path + '/List/save_3ver2'

# Path to the list of reducible configurations
name_red_penta = path + '/Red/reducibles_penta_12'
name_red_tri = path + '/Red/reducibles_tri_12'
name_red_3ver = path + '/Red/reducibles_3ver_12'

#####################
## Save from files ##
#####################

# Returns a list of words saved in a file named 'name'
def find_configs(name):
    fr = open(name, 'r')
    res = []
    L = fr.readlines()
    for s in L :
        res.append(s[:-1])
    return res

######################################
## List of reducible configurations ##
######################################

# Returns the list of all the possiblities where the configuration 'r' appears according to the alphabet 'alph'. (3?3 could be 3S3, 3T3, 3P3, 3H3).
def del_dot(r, alph) :
    if r[0] in alph[0] :
        dec = 0
    else :
        dec = 1
    res = ['']
    for i in range(len(r)) :
        if r[i] == '?' :
            l = []
            for s in res :
                l += [s + a for a in alph[(i + dec)%2]]
            res = l
        else :
            res = [s + r[i] for s in res]
    return res

# Returns the list of all the possibilities of each configuration 'r' according to 'alph'.
def replace_dot(red_list, alph):
    res = []
    for r in red_list :
        if '?' in r :
            res += del_dot(r, alph)
        else :
            res.append(r)
    return res


##################################
## Filtering the configurations ##
##################################
# Filters the forbidden configurations, removing substrings, symmetries...
def filter_red(Red) :
    for r in Red :
        if r != '' :
            r1 = r[::-1]
            for i in range(len(Red)) :
                if r != Red[i] and (r in Red[i] or r1 in Red[i]) :
                    Red[i] = ''
    res = []
    for s in Red :
        if s != '' :
            res.append(s)
    return res

# Returns True iff 'sub' substring of 'main' (cyclic).
def sub_string(main, sub) :
    main += main
    return sub in main

# Removes the forbidden configurations from our list of configurations.
def filter_config(Configs, Red) :
    C = []
    for c in Configs :
        if not (any (sub_string(c, s) or sub_string(c, s[::-1]) for s in Red)) :
            C.append(c)
    return C


###########################
## Dictonnary of charges ##
###########################

# Returns a dictionnary which matches an index for each possible transfer of charge.
def dico_weight(giv, tak, adj, ver):
    iterator = 1
    dico_w = {}
    for g in giv :
        for t in tak :
            for l in adj : #weigths of vertices
                for r in adj :
                    dico_w[g+l+t+r] = iterator
                    iterator +=1
        for l1 in adj :  #weigths of faces
            for l2 in ver :
                for r1 in adj :
                    for r2 in ver :
                        dico_w[g+l1+l2+'T'+r2+r1] = iterator
                        iterator +=1
    return dico_w

# Removes symmetry from the dictionnary, symmetric transfers match the same index.
def del_sym_dico(dic) :
    for a in dic  :
        for b in dic :
            if not dic[a] == dic[b] and a[0] == b[0] and a[1:] == b[1:][::-1] :
                dic[b] = dic[a]
    return dic

#########################
## From word to charge ##
#########################

# Returns the lower case of a character 'a' if it is an upper case.
def min_chr(a) :
    return chr(ord(a)+32)
# Returns the upper case of a character 'a' if it is a lower case.
def maj_chr(a):
    return chr(ord(a)-32)

# Returns the maching index in 'dic' where the faces or vertex 's' transfers a charge to 's[j]'.
def index_of (dic, s, j) :
    if len(s) == 3 :
        return dic[s[j] + s[(j+1)%(len(s))] + '3' + s[(j-1)%(len(s))]]
    if len(s) == 6 :
        if j%2 == 0 :
            return dic[maj_chr(s[j])+s[(j+1)%(len(s))]+'t'+s[(j-1)%(len(s))]]
        else :
            return dic[s[j] + maj_chr(s[(j+1)%(len(s))]) + min_chr(s[(j+2)%(len(s))])+'T'
                            + min_chr(s[(j-2)%(len(s))]) + maj_chr(s[(j-1)%(len(s))])]
    if len (s) == 10 :
        if j%2 == 0 :
            return dic['P'+s[(j-1)%(len(s))]+s[j]+s[(j+1)%(len(s))]]
        else :
            return dic['P'+s[(j-2)%(len(s))]+s[(j-1)%(len(s))]+s[j]+s[(j+1)%(len(s))]+s[(j+2)%(len(s))]]


#############################
## From word to inequation ##
#############################

# Returns the constraint (inequation) on a pentagon 's' according to 'alpha' and the transferred charges indexed with 'dic'.
def p_conf_to_eq(s, taker, dic) :
    Eq = [1.] + [0. for i in range(len(dic))]
    for j in (range(len(s))) :
        if s[j] in taker :
            Eq[index_of(dico, s, j)] += 1
    return Eq

# Returns the constraint (inequation) on a triangle 's' according to 'alpha' and the transferred charges indexed with 'dic'.
def tri_conf_to_eq(s, giv, dic) :
    Eq = [1.] + [0. for i in range(len(dic))]
    for j in (range(len(s))) :
        if s[j] in giv  :
            Eq[index_of(dico,s ,j)] -= 1
    return Eq


# Returns the constraint (inequation) on a 3-vertex 's' according to 'alpha' and the transferred charges indexed with 'dic'.
def ver_conf_to_eq(s, giv, dic) :
    Eq = [1.] + [0. for i in range(len(dic))]
    for j in (range(len(s))) :
        if s[j] in giv :
            Eq[index_of(dico, s, j)] -=1
    return Eq



######################
## Printing results ##
######################

# Prints the results, with the value of alpha and each transfer of charge.
def print_nice(sol, dic, ndig) :
    print('alpha : ',round(sol['x'][0],ndig))
    for i in range(1,len(sol['x'])) :
        s = ''
        for g in dic :
            if s == '' and dic[g] == i :
                s +=  g + ' : '
        print (s, round(sol['x'][i], ndig))


##############################
## Analysis of the solution ##
##############################

# Returns the index of the equation that are tight (ie. less than 'eps') in 'sol'.
def chech_sys(sol, eps) :
    res = []
    for i in range(len(sol['s'])) :
        if sol['s'][i] < eps :
            res.append(i)
    return res

# Returns the set of all possible configurations matching an equation.
def set_configs(Cp, Ct, C3, dic) :
    res = Cp + Ct + C3
    for g in dic :
        if g[0] == 'H' :
            res.append(g[1:])
    return res

# Returns the set of all the possible configurations of pentagons, triangle and 3-vertices
def set_configs2(Cp, Ct, C3) :
    res = Cp + Ct + C3
    return res

# Removes the index of equivalent configurations which are tight and useless tight configurations.
def eq_pb (l, S) :
    res = []
    eqs = []
    for i in range(len(l)) :
        if any(w != 0. for w in S[l[i]][1:]) :
            if all(any(S[l[i]][j] != eq[j] for j in range(len(eq))) for eq in eqs) :
                res.append(l[i])
                eqs.append(S[l[i]])
    return res


# Returns all the configurations in 'C' matching the indexes in 'l'.
def config_pb(l, C) :
    res = []
    for i in l :
        if i < len(C) :
            res.append(C[i])
    return res


##########
## Main ##
##########

alph = [['3', 't', 's', 'p', 'h'], ['T', 'S', 'P', 'H']]  #Alphabet on the vertices and then on the faces


# Store the faces that give and receive charges.
giver2 = ['P' , 'p', 'H', 'h']
giver = ['P', 'H']
adjs = ['T', 'S', 'P', 'H']
vert = ['t', 's', 'p', 'h']
taker = ['3', 't', 'T']


## Lists in files
# Stores the lists of all the configurations and the lists of forbidden configurations.
Pentagons = find_configs(name_pent)
Triangles = find_configs(name_tri)
Vertices3 = find_configs(name_3ver)

Red_pent = find_configs(name_red_penta)
Red_tri = find_configs(name_red_tri)
Red_3ver = find_configs(name_red_3ver)

## Filter of the forbidden configurations
# Filters the list of forbidden configurations.
Red_pent2 = filter_red(replace_dot(Red_pent, alph))
Red_tri2 = filter_red(replace_dot(Red_tri, alph))
Red_3ver2 = filter_red(replace_dot(Red_3ver, alph))


print('Reducible filtered')
## Remove the reducible configurations
# Removes the reducible configurations from our lists.
Configs_penta = filter_config(Pentagons, Red_pent2)
Configs_tri = filter_config(Triangles, Red_tri2)
Configs_3ver = filter_config(Vertices3, Red_3ver2)

print('Configurations filtered')


## Dictionnary
#Stores the dictionnary of the charges.
dico = del_sym_dico(dico_weight(giver, taker, adjs, vert))


print('Dictionnary created')

## Generation of the system
Sys = []
Vec = []


# Stores the constraints from each configuration in 'Sys' and 'Vec'
for i in range(len(Configs_penta)) :
    Sys.append(p_conf_to_eq(Configs_penta[i], taker, dico))
    Vec.append(5.)
for i in range(len(Configs_tri)) :
    Sys.append(tri_conf_to_eq(Configs_tri[i], giver2, dico))
    Vec.append(3.)
for i in range(len(Configs_3ver)) :
    Sys.append(ver_conf_to_eq(Configs_3ver[i], giver2, dico))
    Vec.append(3.)


print('System created')

## New dictionnary to understand the solution
# Removes the transfers that do not appear in the system

new_dico = dico.copy()
to_del = []

for i in range(len(Sys[0])-1, -1, -1) :
    if all(eq[i] == 0. for eq in Sys) :
        for eq in Sys :
            del eq[i]
        for g in new_dico :
            if new_dico[g] == i :
                new_dico[g] = -1
                to_del.append(g)
            else :
                if i < new_dico[g] :
                    new_dico[g] -= 1

for g in to_del :
    del new_dico[g]



# Adds the constraints for hexagons and more.
for g in new_dico :
    if g[0] == 'H' :
        eq = [0. for i in range(len(Sys[0]))]
        eq[new_dico[g]] = 1.
        Sys.append(eq)
        g0 = g[1:]
        if len(g0) == 3 :
            if g0[1] == 't' :
                if g0[0] == 'S' or g0[2] == 'S' :
                    Vec.append(1/3)
                else :
                    Vec.append(0.)
            if g0[1] == '3' :
                Vec.append(2/3)
        if len(g0) == 5 :
            if g0[1] == 't' or g0[3] == 't' :
                Vec.append(2/3)
            else :
                if (g0[1] == 's' and g0[0] == 'S') or (g0[3] == 's' and g0[4] == 'S') :
                    Vec.append(2/3)
                else :
                    Vec.append(1/3)


print('Dictionnary updated\n')





## Save
# Saves the system and dictionnary in variables.
Sys_save = Sys[:]
dico_save = new_dico.copy()


## LP Solver
# Solves the linear programm.
Sys = np.array(Sys)

A = matrix(Sys)
b = matrix(Vec)
c = matrix([-1.0]+ [0. for i in range(len(Sys[0])-1)])

sol=solvers.lp(c,A,b, solver = 'glpk')

# Print the solutions with 3 decimal digits.
print_nice(sol, new_dico, 3)


## Analysis
# Stores the tight constraints in the variable pb.
tight = chech_sys(sol, 0.0001)

set = set_configs2(Configs_penta, Configs_tri, Configs_3ver)

pb = config_pb(eq_pb(tight, Sys), set)

