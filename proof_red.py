import numpy as np
from igraph import *
import copy

# Main Path
path = '.'

#The path of the files where there is the list of reducible configurations.
name_red_penta = path + '/Red/reducibles_penta_12'
name_red_tri = path + '/Red/reducibles_tri_12'

#####################
## Save from files ##
#####################

# Returns a list of words saved in a file named 'name'
def find_configs(name):
    fr = open(name, 'r')
    res = []
    L = fr.readlines()
    for s in L :
        res.append(s[:-1])
    return res


# Returns True iff 's' is not empty
def not_empty(s) :
    return s != ''

########################
## From word to graph ##
########################

# Adds vertices in 'G' at the place 'i' according to the character 'c', representing a vertex or a face.
def chr_to_vert(c, G, i) :
    if c == 't' :
       G.append([i,len(G)-1])
    if c =='s' :
        G.append([len(G)-1])
        G.append([i, len(G) -1])
    if c == 'T' :
        G[-1].append(i)
    if c == 'S' :
        G.append([i, len(G) - 1])
    if c == 'P':
        G.append([len(G)-1])
        G.append([i, len(G)-1])
    if c == '?' :
        G.append([i])

# Returns a list of edges from a list of adjacency 'G'.
def list_tuple(G) :
    T = []
    for i in range(len(G)) :
        for j in G[i]:
            if j < i :
                T.append([j, i])
    return T

# Returns a graph in line, represented by the word 's'.
def word_to_lines(s):
    G = [[1]] + [[i-1, i+1] for i in range(1,len(s)//2)] + [[len(s)//2-1]]
    G.append([0])
    chr_to_vert(s[0], G, 0)
    for i in range(1,(len(s)//2) + 1) :
        chr_to_vert(s[2*i-1], G, i)
        if 2*i < len(s) :
            chr_to_vert(s[2*i], G, i)
    return G

# Normalizes words, they must begin with lower case.
def norm_word(s) :
    if s[0] == 'S' or s[0] == 'T' :
        s = '?' + s
    return s

# Closes a graph in line 'g' to make a cycle of the length of the string 's'.
def poly_graph(s, g) :
    map = [i for i in range(g.vcount())]
    map[len(s)//2] = 0
    map[-1] = len(s)//2 + 1
    g.contract_vertices(map)
    g.simplify()

# Adds the 3-vertex attribute to the right vertices in 'g' according to the string 's'.
def atrr_3ver(s, g) :
    g.vs['is_3vertex'] = [False for v in g.vs]
    for i in range(0, len(s), 2) :
        if  s[i] == '3' :
            g.vs[i//2]['is_3vertex'] = True

            
# Removes the isolated vertices and the ones not in the configuration.
def clear_graph(g) :
    g.delete_vertices(g.vs.select(_degree_le=1))

# Returns a graph according to a word 's' in the form given by 'a' : l = line, t = triangle, p = pentagon
def words_to_graph(s, a) :
    assert s != '', 'the word is empty'
    s = norm_word(s)
    if a == 't' :
        assert len(s) < 7, 'word too long for a triangle shape'
        s = (s+'??????')[:6]
    if a == 'p' :
        assert len(s) < 11, 'word too long for a pentagon shape'
        s = (s+'??????????')[:10]
    T = list_tuple(word_to_lines(s))
    g = Graph.TupleList(T)
    if a == 't' or a == 'p' :
        poly_graph(s, g)
    atrr_3ver(s, g)
    clear_graph(g)
    return g

#####################################
## Lists of colors/ Initialisation ##
#####################################

# Computes the number of already colored direct neighbors of 'v' in 'g'.
def direct_colored_vertices(v, g) :
    if v['is_3vertex'] :
        return (3 - v.degree())
    else :
        return (4 - v.degree())

# Computes the number of already colored neighbors of 'v' in the square of 'g'.
def colored_vertices(v, g) :
    taken = 4*(direct_colored_vertices(v, g))
    for id in g.neighbors(v) :
        taken += direct_colored_vertices(g.vs[id], g)
    return taken


# Initializes the attributes on the vertices of 'g' : their name, their number of free colors using 'N' colors matching the function l, and their list of inclusions.
def attr_init(g, N) :
    g.vs['name'] = [i for i in range(g.vcount())]
    g.vs['colors'] = [N - colored_vertices(v, g) for v in g.vs]
    g.vs['included_in'] = [[v['name']] for v in g.vs]


#######################
## Square of a graph ##
#######################

# Returns the square of the graph 'g'
def square(g) :
    g2 = copy.deepcopy(g)
    for id1 in range(g.vcount()) :
        for id2 in g.neighbors(g.vs[id1]) :
            for id3 in g.neighbors(g.vs[id2]) :
                g2.add_edges([(id1, id3)])
    g2.simplify()
    return g2

###################
## Good vertices ##
###################

# Finds the happy vertices in 'g'.
def find_happy(g) :
    res = []
    for v in g.vs :
        if v.degree() < v['colors'] :
            res.append(v)
    return res

# Finds the 'almost' happy vertices in g, whose lists are equal or greater than their degree.
def find_almost_happy(g) :
    res = []
    for v in g.vs :
        if v.degree() <= v['colors'] :
            res.append(v['name'])
    return res

# Colors the happy vertices in 'g', wich we can color without touching their neighborhood, until there is none.
def del_happy(g) :
    while find_happy(g) != [] :
        g.delete_vertices(find_happy(g))

##################
## Easy updates ##
##################

# Colors the vertex 'v' in the graph 'g'.
def color_vertex(v, g) :
    for id in g.neighbors(v) :
        g.vs[id]['colors'] -= 1
    g.delete_vertices(v)

# Colors all the vertices with a list of length 1 in 'g' until there is none.
def color_the_ones(g) :
    while 1 in g.vs['colors'] :
        color_vertex(g.vs[g.vs['colors'].index(1)], g)

# Colors the vertices which are happy or which has only one choice.
def easy_update(g) :
    color_the_ones(g)
    del_happy(g)


######################
## Naive coloration ##
######################

# Returns True iff a vertex cannot be colored anymore in 'g'.
def zero_colors(g) :
    for v in g.vs :
        if v['colors'] <= 0 :
            return True


# Return True if we know that the graph 'g' is easily colorable. We manually check the 'difficult' case, but it do not occurs yet.
def good_end(g) :
    m = len(find_almost_happy(g))
    if g.vcount() == 0 :
        return True
    return False

#########################
## Handling inclusions ##
#########################

# Returns a copy of 'g' where we have colored 'v0' with a color which is not inside the list of 'v1'.
def is_included_in_step(v0, v1, g) :
    gc = copy.deepcopy(g)
    v0 = gc.vs.find(name=v0['name'])
    v1 = gc.vs.find(name=v1['name'])
    for id in gc.neighbors(v0) :
        if not(v1['name'] in gc.vs[id]['included_in']) :
            gc.vs[id]['colors'] -= 1
    gc.delete_vertices(v0)
    return gc


# Returns True if the graph 'g' is empty (we can color 'g' naively) or the lists of inclusions bring a contradiction
def incl_contra(g) :
    if g.vcount() == 0 :
        return True
    if zero_colors(g) :
            return False
    for v in g.vs :
        for n in v['included_in'] :
            if n in g.vs['name'] :
                u = g.vs.find(name=n)
#                if v['colors'] > u['colors'] :
#                    return True
                if v['colors'] > g.degree(u) :
                    g2 = copy.deepcopy(g)
                    g2.delete_vertices(u)
                    if l_choosable(g2):
                        return True
    return False

#########################
## Vertex choosability ##
#########################

# Returns the minimum index of the minimum of a list
def min_index(l) :
    return l.index(min(l))


# Updates 'g', and computes a matrix of inclusions of '(g, l)', 'l' represented by the attribute 'colors'. Only computes inclusions in 'almost happy' vertices.
def l_choosable_step(g) :
    easy_update(g)
    Pn = find_almost_happy(g)
    P = [g.vs.find(name=n) for n in Pn]
    for v in P :
        list_changed = True
        while list_changed :
            list_changed = False
            I = g.neighbors(v)
            C = [g.vs[id]['colors'] for id in I]
            while I != [] :
                i = 0
                id = I[i]
                del I[i]
                del C[i]
                gc = is_included_in_step(g.vs[id], v, g)
                if  (not v['name'] in g.vs[id]['included_in']) and l_choosable(gc) :
                    list_changed = True
                    g.vs[id]['included_in'] += [v['name']]


# If it returns True, 'g' is l-choosable, with l the attribute named 'colors'.
def l_choosable(g) :
    l_choosable_step(g)
    return incl_contra(g)

#############
## Drawing ##
#############

# Functions which draw our graphs with some informations.

## Codes:
# Background color: 3-vertices are green, 4-vertices are blue
# Text color: happy vertices -> red, almost happy -> pink, others -> black
# Text = (name; number of available colors)

# Returns label color for each vertex.
def visual_label_color(P, C, g) :
    c = 'red' # red vertices are happy vertices 
    p = 'pink' # pink vertices are almost happy vertices
    b = 'black' # otherwise the vertex is black
    res = []
    for v in g.vs :
        if v in C :
            res.append(c)
        else :
            if v['name'] in P :
                res.append(p)
            else :
                res.append(b)
    return res

# Draws the graph 'g' when 'g' is a square.
def draw_nice_square(g) :
    color_dict = {True : "green", False : "blue"}
    P =  find_almost_happy(g)
    C = find_happy(g)
    visual_style = {}
    visual_style["vertex_size"] = 50
    visual_style["vertex_color"] = [color_dict[b] for b in g.vs["is_3vertex"]]
    visual_style["vertex_label"] = [chr(97 + v["name"]) + ';' + str(v["colors"]) for v in g.vs]
    visual_style["layout"] = g.layout()
    visual_style["vertex_label_size"] = 26
    visual_style["vertex_label_color"] = visual_label_color(P, C, g)
    plot(g, **visual_style)

# Draws the graph 'g'.
def draw_nice(g) :
    color_dict = {True : "green", False : "blue"} # 3-vertices are green, 4-vertices are blue
    g2 = square(g)
    P =  find_almost_happy(g2)
    C = find_happy(g2)
    visual_style = {}
    visual_style["vertex_size"] = 50
    visual_style["vertex_color"] = [color_dict[b] for b in g.vs["is_3vertex"]]
    visual_style["vertex_label"] = [chr(97 + v.index) + ';'+ str(v["colors"]) for v in g.vs]
    visual_style["layout"] = g.layout()
    visual_style["vertex_label_size"] = 26
    visual_style["vertex_label_color"] = visual_label_color(P, C, g2)
    plot(g, **visual_style)


##########
## Main ##
##########


## Lists in files
# Stores our lists of forbidden configuration which are saved in files. All the forbidden configurations involving 3-vertices match another configuration involving a triangle or a pentagon. Thus, we just need to reduce the configurations with triangles and pentagons.

Red_pent = list(filter(not_empty, find_configs(name_red_penta)))
Red_tri = list(filter(not_empty, find_configs(name_red_tri)))
### List of graphs
# Transforms our lists of words into lists of graphs.

G_pent = [words_to_graph(s, 'l') for s in Red_pent]
G_pent_p = [words_to_graph(s, 'p') for s in Red_pent]
G_tri = [words_to_graph(s, 't') for s in Red_tri]


## Initialisation
#Initializes the attributes of each vertex of each graph.

for i in range(len(G_pent)) :
    attr_init(G_pent[i], 12)
    attr_init(G_pent_p[i], 12)

for i in range(len(G_tri)) :
    attr_init(G_tri[i], 12)


## Reducible configurations with inclusions
# Stores in the list 'good' the configurations that our algorithm reduces and print the number of reduced configurations.

good = []
goodtri = []

# For pentagons, we first try to reduce without closing the face.
for i in range(len(G_pent)):
    g = copy.deepcopy(G_pent[i])
    g2 = square(g)
    if l_choosable(g2) :
        good.append(i)
    else :
        g = copy.deepcopy(G_pent_p[i])
        g2 = square(g)
        if l_choosable(g2) :
            good.append(i)

for i in range(len(G_tri)) :
    g = copy.deepcopy(G_tri[i])
    g2 = square(g)
    if l_choosable(g2) :
        goodtri.append(i)


##############################
## Remaining configurations ##
##############################

# Draws the remaining configurations as pentagons and print their index, their matching word and their matrix of inclusions computed.

print('Remaining configurations : ')
print('Type Index Configuration_name Matrix_of_inclusions')


for i in range (len(G_tri)) :
    if not i in goodtri :
        g = copy.deepcopy(G_tri[i])
        g2 = square(g)
        l_choosable_step(g2)
        print("Triangle", i, Red_tri[i], g2.vs['included_in'])
        # Uncomment the following line to draw the reamining configurations
        # draw_nice(g)

for i in range (len(G_pent)) :
    if not i in good :
        g = copy.deepcopy(G_pent_p[i])
        g2 = square(g)
        l_choosable_step(g2)
        print("Pentagon", i, Red_pent[i], g2.vs['included_in'])
        # Uncomment the following line to draw the reamining configurations
        # draw_nice(g)
