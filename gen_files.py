#################
## Description ##
#################

# The goal of this program is to generate (up to symmetries) all possible "neighborhoods" of a 3-vertex, 3-face or 5-face. More precisely, we remember only the information needed to determine which rules will apply.

# The neighborhood of a 3-vertex is given by a 3-letter word on the alphabet [T,S,P,H], representing the sizes of its three incident faces (T = triangle/3-face, S = square/4-face, P = pentagon/5-face, H = 6^+-face). 

# 3-faces uvw are represented by a 6-letter word a_1...a_6 where the even-indexed characters represent the length of the faces edge-incident with uv, vw, uw (using the same convention as above). Odd-indexed characters lie in [3,t,s,p,h] and they reprensent the degree ofu, v and w. 3 means that the vertex has degree 3, while t,s,p,h mean that the vertex has degree 4 and we also get some information on the length of the opposite face (reusing again the same convention). Note that we use uppercase symbols for faces that share an edge with uvw, while lower case symbols are used for faces that share only a vertex with uvw.

# 5-faces are represented similarly, but with a 10-letter word.

import numpy as np

#######################
## Path of the files ##
#######################

# Main path
path = '.'

# The path to save our list of configurations.
name_pent = path + '/List/save_penta2a'          #Lists of pentagons
name_tri = path + '/List/save_tri2a'            #Lists of triangles
name_3ver = path + '/List/save_3ver2a'          #Lists of 3-vertices

################################
## Generation of the language ##
################################

# Returns the words of length 'n' in our language with alphabet 'alph'.
def conf_rec(n, alph) :
    assert n >= 0,  'n strict pos'
    if n == 0 :
        return ['']
    L = conf_rec(n-1, alph)
    G = []
    for i in range(len(L)) :
        for a in alph[(n+1)%2] :
            G.append(L[i] + a)
    return G

#####################
## Symmetry filter ##
#####################

# Rolls the string 's' 'i' times.
def roll_str(s, i):
    return ''.join(np.roll(list(s), i))

# Returns all the possible rotation and symmetric rotation of 's' with a step of 2. Here, we do not need a step of one, because all our words begins with a vertex.
def gen_sym_step2(s) :
    sym = []
    s_0, s_1 = s[:], s[::-1]
    for i in range(0, len(s), 2) :
        sym.append(roll_str(s_0, i))
        sym.append(roll_str(s_1, i + 1))
    return sym

# Removes the equivalent string by rotation or symmetric rotation in our list of words 'C'.
def del_sym(C) :
    c = 0
    print(len(C))
    res = []
    for i in range(len(C)) :
        if min(gen_sym_step2(C[i])) == C[i] :
            res.append(C[i])
        if c <= i :
            c += 2000
            print(i)
    return res

#####################
## 3-vertex filter ##
#####################

# The configurations of a 3-vertices just need to know the incident faces, so we delete the adjacent vertices of our words then we filter the results once again.

# Returns the possible rotation and symmetric rotation of 's' with a step of 1. Here, we have removed the vertex so we need a step of 1.
def gen_sym_step1(s) :
    sym = []
    s_0, s_1 = s[:], s[::-1]
    for i in range(0, len(s)) :
        sym.append(roll_str(s_0, i))
        sym.append(roll_str(s_1, i))
    return sym

# Removes the equivalent string by rotation or symmetric rotation in our list of words 'C'.
def del_sym1(C) :
    res = []
    for i in range(len(C)) :
        if min(gen_sym_step1(C[i])) == C[i] and not C[i] in res :
            res.append(C[i])
    return res

#####################
## Pentagon filter ##
#####################

# For the pentagons, we remove the configuration where there is no receivers (triangles and 3-vertices).

# Removes the configurations where there are no receivers.
def del_useless(C) :
    res = []
    for c in C:
        if ('3' in c or 't' in c or 'T' in c) :
            res.append(c)
    return res

#################
## File saving ##
#################

# Saves 'C' in a file named 'name'
def save_configs(name, C):
    fw = open(name, 'w')
    for s in C :
        fw.write(s+'\n')
    fw.close()

# Returns a list of words saved in a file named 'name'
def find_configs(name):
    fr = open(name, 'r')
    res = []
    L = fr.readlines()
    for s in L :
        res.append(s[:-1])
    return res

##########
## Main ##
##########


## Variables
alph = [['3', 't', 's', 'p', 'h'], ['T', 'S', 'P', 'H']]    #Alphabet on the vertices and then on the faces


# Generates of all the pentagons, the triangles and the 3-vertices with symmetry.

C_penta = conf_rec(10, alph)
C_tri = conf_rec(6, alph)
C_3ver = conf_rec(6, alph)

# Filters the symmetry of the pentagons/triangles/3-vertices.

Cp_sorted = del_sym(C_penta)
Ct_sorted = del_sym(C_tri)
Cv_sorted = del_sym (C_3ver)

# Filters the 3-vertices without the adjacent vertices.
Cv_sorted = del_sym1([s[1::2] for s in Cv_sorted])

# Filters the useless configurations.
Cp_useful = del_useless(Cp_sorted)

# Saves the results in the right files.

save_configs(name_pent, Cp_sorted)
save_configs(name_tri, Ct_sorted)
save_configs(name_3ver, Cv_sorted)
