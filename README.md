# Contents of the repository

## Generating and storing all possible configurations

The file `gen_files.py` generates the lists of all possible patterns that may occur around 5-faces (pentagons), 3-faces (triangles) and 3-vertices, up to symmetry. The generation of the pentagons is quite long so we saved our lists in the files of [List]. Once our lists are saved, we do not need to run this program anymore. Note that the format of the configurations is slightly different from the one used in the article. The notation is formally described in gen_files.py.

## Reducible configurations

The files of [Red] are manually generated: they contain the list of patterns that cannot occur (because of reducible configurations) on a pentagon, triangle and 3-vertices. The notation follows the same convention as for the files in [List], but we added a wildcard character "?".

There are 53 such patterns, scattered in the three files. Note that this does not correspond to the number (40) of configurations, since the same configuration may yield several patterns. For example, the reducible configuration from Lemma 5.6 (a 3-vertex on a triangle) yields the pattern T around a 3-vertex, the pattern 3T around a pentagon, and the pattern 3 around a triangle. Moreover, a configuration can yield several patterns in the same file: for example, the third configuration from Figure 5 yields four patterns (TtS and tTs around a pentagon, sT and T?S around a triangle).

## Linear program

The file `solve_sys.py` filters our lists in [List], removing all the patterns giving rise to a reducible configuration, and then solves our linear program. Once executed, it prints the results of the linear program: the value of alpha and the value of the charge to transfer. Also, we store in the variable `pb` the list of tight configurations.

## Final discharging rules

The discharging rules are given in the `rules` file, with the following formatting (where d,l,m,r,s are characters representing lengths of faces as explained in gen_files.py):

dl3r : w_{d,l,3v,r} (a d-face to an incident 3-vertex)
dltr : w_{d,l,3,r} (a d-face to a vertex-incident triangle)
dlmTsr : w_{d,l,m,3,s,r} (a d-face to an edge-incident triangle)

Note that when d = 'H', we obtain the rules T1-T6.

## Reducing configurations

The file `proof_red.py` contains the algorithm on l-choosability and it proves that most of the configurations in our lists in [Red] are reducible. The remaining ones (that are not proved by our algorithm) are printed with their matrix of inclusions. We also list them in the file `remaining`, together with the lemma reducing them in the paper. Note that the file contains 15 lines while only 12 configurations are reduced in the paper, but recall that some configurations may appear several times (e.g. "3" on triangles and "3T" on pentagons).
